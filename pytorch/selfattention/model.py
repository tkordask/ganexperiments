import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

from typing import Optional, TypeVar

T = TypeVar('T', bound='nn.Module')


class Generator(nn.Module):
    def __init__(self):
        super(Generator, self).__init__()
        self.t32x32_1 = nn.Sequential(
            nn.TransformerEncoderLayer(32, 4, 32, 0),
            nn.LayerNorm((32, 32))
        )
        self.t32x32_2 = nn.Sequential(
            nn.TransformerEncoderLayer(32, 4, 32, 0),
            nn.LayerNorm((32, 32))
        )

        self.t32x64_1 = nn.Sequential(
            nn.TransformerEncoderLayer(32, 4, 32, 0),
            nn.LayerNorm((64, 32))
        )
        self.t32x64_2 = nn.Sequential(
            nn.TransformerEncoderLayer(32, 4, 32, 0),
            nn.LayerNorm((64, 32))
        )

        self.t32x128_1 = nn.Sequential(
            nn.TransformerEncoderLayer(32, 4, 32, 0),
            nn.LayerNorm((128, 32))
        )
        self.t32x128_2 = nn.Sequential(
            nn.TransformerEncoderLayer(32, 4, 32, 0),
            nn.LayerNorm((128, 32))
        )

        self.t32x256_1 = nn.Sequential(
            nn.TransformerEncoderLayer(32, 4, 32, 0),
            nn.LayerNorm((256, 32))
        )
        self.t32x256_2 = nn.Sequential(
            nn.TransformerEncoderLayer(32, 4, 32, 0),
            nn.LayerNorm((256, 32))
        )

        self.t32x512_1 = nn.Sequential(
            nn.TransformerEncoderLayer(32, 4, 32, 0),
            nn.LayerNorm((512, 32))
        )
        self.t32x512_2 = nn.Sequential(
            nn.TransformerEncoderLayer(32, 4, 32, 0),
            nn.LayerNorm((512, 32))
        )

        self.t32x1024_1 = nn.Sequential(
            nn.TransformerEncoderLayer(32, 4, 32, 0),
            nn.LayerNorm((1024, 32))
        )
        self.t32x1024_2 = nn.Sequential(
            nn.TransformerEncoderLayer(32, 4, 32, 0),
            nn.LayerNorm((1024, 32))
        )

        self.final = nn.Sequential(
            nn.ConvTranspose2d(32, 3, 4, 2, 1),
            nn.Sigmoid()
        )

    def forward(self, input):

        def upsample(x, size):
            x_t = torch.transpose(x, 1, 2)
            out = nn.functional.interpolate(x_t, size=size)
            return torch.transpose(out, 1, 2)

        out_32x32_1 = self.t32x32_1(input)
        out_32x32_2 = self.t32x32_2(out_32x32_1) + out_32x32_1

        in_32x64 = upsample(out_32x32_2, 64)
        out_32x64_1 = self.t32x64_1(in_32x64)+in_32x64
        out_32x64_2 = self.t32x64_2(out_32x64_1) + out_32x64_1

        in_32x128 = upsample(out_32x64_2, 128)
        out_32x128_1 = self.t32x128_1(in_32x128)+in_32x128
        out_32x128_2 = self.t32x128_2(out_32x128_1) + out_32x128_1

        in_32x256 = upsample(out_32x128_2, 256)
        out_32x256_1 = self.t32x256_1(in_32x256)+in_32x256
        out_32x256_2 = self.t32x256_2(out_32x256_1) + out_32x256_1

        in_32x512 = upsample(out_32x256_2, 512)
        out_32x512_1 = self.t32x512_1(in_32x512)+in_32x512
        out_32x512_2 = self.t32x512_2(out_32x512_1) + out_32x512_1

        in_32x1024 = upsample(out_32x512_2, 1024)
        out_32x1024_1 = self.t32x1024_1(in_32x1024)+in_32x1024
        out_32x1024_2 = self.t32x1024_2(out_32x1024_1) + out_32x1024_1

        in_conv = torch.reshape(torch.transpose(
            out_32x1024_2, 1, 2), (-1, 32, 32, 32))
        out_conv = self.final(in_conv)

        return out_conv

    def init_weights(self):
        self.apply(weights_init)


class Discriminator(nn.Module):
    def __init__(self, ndf=64):
        super(Discriminator, self).__init__()
        self.main = nn.Sequential(
            # input is (nc) x 64 x 64
            nn.Conv2d(3, ndf, 4, 2, 1, bias=False),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf) x 32 x 32
            nn.Conv2d(ndf, ndf * 2, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 2),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*2) x 16 x 16
            nn.Conv2d(ndf * 2, ndf * 4, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 4),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*4) x 8 x 8
            nn.Conv2d(ndf * 4, ndf * 8, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 8),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*8) x 4 x 4
            nn.Conv2d(ndf * 8, 100, 4, 1, 0, bias=False),
        )
        self.linear = nn.Linear(100, 1)

    def forward(self, input):
        after_main = torch.reshape(self.main(input), (-1, 100))
        return self.linear(after_main)

    def init_weights(self):
        self.apply(weights_init)


# custom weights initialization called on netG and netD


class PiecewiseLinear(nn.Module):
    def __init__(self, dims, nodes):
        super(PiecewiseLinear, self).__init__()
        self.num_nodes = nodes
        self.nodes = nn.Parameter(torch.zeros(dims, nodes))

    def forward(self, x):
        sorted_nodes = torch.sort(self.nodes, 1)[0]
        result = torch.abs(x - sorted_nodes[:, 0])
        acc = torch.zeros_like(sorted_nodes[:, 0])
        for i in range(1, self.num_nodes):
            cur = sorted_nodes[:, i]
            delta = cur-sorted_nodes[:, i-1]

            if i % 2 == 1:
                acc = acc+delta

                result_current = -(x-cur) + acc
            else:
                acc = acc - delta
                result_current = (x-cur) + acc
            result = torch.where(
                torch.ge(x, cur), result_current, result)
        return result

    def init_weights(self):
        nn.init.uniform_(self.nodes, -3, 3)


class PiecewiseLinearL2(nn.Module):
    def __init__(self, dims, num_spheres, num_planes):
        super(PiecewiseLinearL2, self).__init__()
        self.dims = dims
        self.num_spheres = num_spheres
        self.num_planes = num_planes

        self.sphere_locs = nn.Parameter(torch.zeros(num_spheres, dims))
        self.sphere_rads = nn.Parameter(torch.zeros(num_spheres))
        self.sphere_consts = nn.Parameter(torch.zeros(num_spheres))

        self.plane_dirs = nn.Parameter(torch.zeros(dims, num_planes)+.0001)
        self.plane_shift = nn.Parameter(torch.zeros(num_planes))
        self.plane_constants = nn.Parameter(torch.zeros(num_planes))

    def forward(self, x):
        expanded_spheres = torch.reshape(
            x, (-1, 1, self.dims)).expand(-1, self.num_spheres, self.dims)
        spheres_shifted = expanded_spheres-self.sphere_locs
        spheres_normed = torch.norm(spheres_shifted, dim=2)

        spheres_sub_radius = torch.abs(spheres_normed-self.sphere_rads**2)
        spheres_plus_consts = spheres_sub_radius + self.sphere_consts

        normed_plane_dirs = self.plane_dirs/torch.norm(self.plane_dirs, dim=0)

        planes_projected = torch.einsum('bd,dp->bp', x, normed_plane_dirs)
        planes_distance = planes_projected - self.plane_shift
        planes_abs_plus_consts = torch.abs(
            planes_distance) + self.plane_constants

        all_distances = torch.cat([
            spheres_plus_consts, planes_abs_plus_consts], 1)
        return torch.min(all_distances, 1)[0]

    def init_weights(self):
        for param in self.parameters():
            nn.init.uniform_(param, -3, 3)


def weights_init(m):
    for param in m.parameters():
        try:
            nn.init.xavier_normal_(param)
        except:
            nn.init.normal_(param, 0, 0.02)


def num_params(model):
    model_parameters = filter(lambda p: p.requires_grad, model.parameters())
    params = sum([np.prod(p.size()) for p in model_parameters])
    return params


#x = torch.ones((1, 32, 32))

#gen = Generator()
# gen.init_weights()

# print(num_params(gen))

# gen(x)


# x = torch.zeros((1, 100, 1, 1))
# G = Generator()
# fakes = G(x)
# print(fakes.size())
# embed = Embedding()
# d = Discriminator()
# embedding = embed(fakes)
# print(embedding.size())
# disc = d(embedding)
# print(disc.size())
# model_parameters = filter(lambda p: p.requires_grad, d.parameters())
# params = sum([np.prod(p.size()) for p in model_parameters])
# print(params)
