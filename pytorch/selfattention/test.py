import torch

import model
import train


gen = model.Generator()
disc = model.Discriminator()
gen.init_weights()
disc.init_weights()


train.train(gen, disc, [[torch.ones((128, 32, 32))]],
            128, 2, torch.device("cpu"))
