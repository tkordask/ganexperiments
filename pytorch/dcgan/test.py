import torch

import model
import train


gen = model.Generator()
embed = model.Embedding()
disc = model.Discriminator()
gen.init_weights()
embed.init_weights()
disc.init_weights()

train.train(gen, embed, disc, [[torch.ones((128*5, 3, 64, 64))]],
            128, 2, torch.device("cpu"))
