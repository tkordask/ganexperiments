import torch
import torch.optim as opt
import torch.nn.functional as F
import torchvision.utils as vutils
import numpy as np
import matplotlib.pyplot as plt


def train(G, E, D, data, batch_size, epochs, device, lr=0.0002, beta1=0.5):
    print("Beginning training!")
    fake_rng = torch.distributions.normal.Normal(
        0, 1).expand((batch_size, 100, 1, 1))

    optG = opt.Adam(G.parameters(), lr=lr, betas=(beta1, 0.999))
    optD = opt.Adam(list(E.parameters()) + list(D.parameters()),
                    lr=lr, betas=(beta1, 0.999))

    G.train()
    E.train()
    D.train()

    fixed_noise = torch.rand(64, 100, 1, 1).to(device)

    G_losses = []
    D_losses = []
    img_list = []

    for epoch in range(epochs):
        print("begin epoch ", epoch)
        for (i, data_batch) in enumerate(data):
            if data_batch[0].size()[0] < batch_size:
                break
            device_batch = data_batch[0].to(device)

            optD.zero_grad()

            real_embedding = E(device_batch)
            real_output = D(real_embedding)

            fake_batch = fake_rng.sample().to(device)

            fakes = G(fake_batch)

            fakes_embedding = E(fakes)
            fake_output = D(fakes_embedding)

            D_loss = -torch.mean(torch.clamp(torch.log(1-(fake_output)), -100, 100)
                                 ) - torch.mean(torch.clamp(torch.log(real_output), -100, 100))
            D_loss.backward(retain_graph=True)
            optD.step()
            D_x = torch.mean(real_output).item()
            D_G_z1 = torch.mean(fake_output).item()

            G.zero_grad()
            # fakes = G(fake_batch)
            fakes_embedding = E(fakes)
            fake_output = D(fakes_embedding)
            G_loss = - \
                torch.mean(torch.clamp(torch.log(fake_output), -100, 100))
            G_loss.backward()
            optG.step()
            D_G_z2 = torch.mean(fake_output).item()

            # Output training stats
            if i % 50 == 0:
                print('[%d/%d][%d/%d]\tLoss_D: %.4f\tLoss_G: %.4f\tD(x): %.4f\tD(G(z)): %.4f / %.4f'
                      % (epoch, epochs, i, len(data),
                         D_loss.item(), G_loss.item(), D_x, D_G_z1, D_G_z2))

            # Save Losses for plotting later
            G_losses.append(G_loss.item())
            D_losses.append(D_loss.item())

        # Check how the generator is doing by saving G's output on fixed_noise
            if (i % 250 == 0) or ((epoch == epochs-1) and (i == len(data)-1)):
                G.eval()
                with torch.no_grad():
                    fake = G(fixed_noise).detach().cpu()
                new_img = vutils.make_grid(fake, padding=2, normalize=True)
                img_list.append(new_img)
                plt.close()
                plt.figure(figsize=(16, 16))
                plt.imshow(np.transpose(new_img, (1, 2, 0)))
                plt.show()
                G.train()

    return (G_losses, D_losses, img_list)


def loss_hinge_dis(dis_fake, dis_real):
    loss = torch.mean(F.relu(1. - dis_real))
    loss += torch.mean(F.relu(1. + dis_fake))
    return loss


def loss_hinge_gen(dis_fake):
    loss = -torch.mean(dis_fake)
    return loss


def loss_dcgan_dis(dis_fake, dis_real):
    L1 = torch.mean(F.softplus(-dis_real))
    L2 = torch.mean(F.softplus(dis_fake))
    return L1 + L2


def loss_dcgan_gen(dis_fake):
    loss = torch.mean(F.softplus(-dis_fake))
    return loss
