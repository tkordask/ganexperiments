import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

from typing import Optional, TypeVar

T = TypeVar('T', bound='nn.Module')


class Generator(nn.Module):
    def __init__(self, nz=100, ngf=64):
        super(Generator, self).__init__()
        self.main = nn.Sequential(
            # input is Z, going into a convolution
            nn.ConvTranspose2d(nz, ngf * 8, 4, 1, 0, bias=False),
            nn.BatchNorm2d(ngf * 8),
            nn.ReLU(True),
            # state size. (ngf*8) x 4 x 4
            nn.ConvTranspose2d(ngf * 8, ngf * 4, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ngf * 4),
            nn.ReLU(True),
            # state size. (ngf*4) x 8 x 8
            nn.ConvTranspose2d(ngf * 4, ngf * 2, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ngf * 2),
            nn.ReLU(True),
            # state size. (ngf*2) x 16 x 16
            nn.ConvTranspose2d(ngf * 2, ngf, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ngf),
            nn.ReLU(True),
            # state size. (ngf) x 32 x 32
            nn.ConvTranspose2d(ngf, 3, 4, 2, 1, bias=False),
            nn.Sigmoid()
            # state size. (nc) x 64 x 64
        )

    def forward(self, input):
        return self.main(input)

    def init_weights(self):
        self.apply(weights_init)


class Embedding(nn.Module):
    def __init__(self, ndf=64):
        super(Embedding, self).__init__()
        self.main = nn.Sequential(
            # input is (nc) x 64 x 64
            nn.Conv2d(3, ndf, 4, 2, 1, bias=False),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf) x 32 x 32
            nn.Conv2d(ndf, ndf * 2, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 2),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*2) x 16 x 16
            nn.Conv2d(ndf * 2, ndf * 4, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 4),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*4) x 8 x 8
            nn.Conv2d(ndf * 4, ndf * 8, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 8),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*8) x 4 x 4
            nn.Conv2d(ndf * 8, 100, 4, 1, 0, bias=False),
        )

    def forward(self, input):
        out = self.main(input)
        return torch.reshape(out, (-1, 100))

    def init_weights(self):
        self.apply(weights_init)


class Discriminator(nn.Module):
    def __init__(self,):
        super(Discriminator, self).__init__()
        self.main = nn.Sequential(
            nn.Linear(100, 50),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Linear(50, 25),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Linear(25, 1)
        )

    def forward(self, input):

        return self.main(input)

    def init_weights(self):
        self.apply(weights_init)

# custom weights initialization called on netG and netD


class PiecewiseLinear(nn.Module):
    def __init__(self, dims, nodes):
        super(PiecewiseLinear, self).__init__()
        self.num_nodes = nodes
        self.nodes = nn.Parameter(torch.zeros(dims, nodes))

    def forward(self, x):
        sorted_nodes = torch.sort(self.nodes, 1)[0]
        result = torch.abs(x - sorted_nodes[:, 0])
        acc = torch.zeros_like(sorted_nodes[:, 0])
        for i in range(1, self.num_nodes):
            cur = sorted_nodes[:, i]
            delta = cur-sorted_nodes[:, i-1]

            if i % 2 == 1:
                acc = acc+delta

                result_current = -(x-cur) + acc
            else:
                acc = acc - delta
                result_current = (x-cur) + acc
            result = torch.where(
                torch.ge(x, cur), result_current, result)
        return result

    def init_weights(self):
        nn.init.uniform_(self.nodes, -3, 3)


class PiecewiseLinearL2(nn.Module):
    def __init__(self, dims, num_spheres, num_planes):
        super(PiecewiseLinearL2, self).__init__()
        self.dims = dims
        self.num_spheres = num_spheres
        self.num_planes = num_planes

        self.sphere_locs = nn.Parameter(torch.zeros(num_spheres, dims))
        self.sphere_rads = nn.Parameter(torch.zeros(num_spheres))
        self.sphere_consts = nn.Parameter(torch.zeros(num_spheres))

        self.plane_dirs = nn.Parameter(torch.zeros(dims, num_planes)+.0001)
        self.plane_shift = nn.Parameter(torch.zeros(num_planes))
        self.plane_constants = nn.Parameter(torch.zeros(num_planes))

    def forward(self, x):
        expanded_spheres = torch.reshape(
            x, (-1, 1, self.dims)).expand(-1, self.num_spheres, self.dims)
        spheres_shifted = expanded_spheres-self.sphere_locs
        spheres_normed = torch.norm(spheres_shifted, dim=2)

        spheres_sub_radius = torch.abs(spheres_normed-self.sphere_rads**2)
        spheres_plus_consts = spheres_sub_radius + self.sphere_consts

        normed_plane_dirs = self.plane_dirs/torch.norm(self.plane_dirs, dim=0)

        planes_projected = torch.einsum('bd,dp->bp', x, normed_plane_dirs)
        planes_distance = planes_projected - self.plane_shift
        planes_abs_plus_consts = torch.abs(
            planes_distance) + self.plane_constants

        all_distances = torch.cat([
            spheres_plus_consts, planes_abs_plus_consts], 1)
        return torch.min(all_distances, 1)[0]

    def init_weights(self):
        for param in self.parameters():
            nn.init.uniform_(param, -3, 3)


def weights_init(m):
    for param in m.parameters():
        try:
            nn.init.xavier_normal_(param)
        except:
            nn.init.normal_(param, 0, 0.02)


x = torch.ones((2, 1))

disc = PiecewiseLinearL2(1, 0, 10)
disc.init_weights()
disc(x)

# x = torch.zeros((1, 100, 1, 1))
# G = Generator()
# fakes = G(x)
# print(fakes.size())
# embed = Embedding()
# d = Discriminator()
# embedding = embed(fakes)
# print(embedding.size())
# disc = d(embedding)
# print(disc.size())
# model_parameters = filter(lambda p: p.requires_grad, d.parameters())
# params = sum([np.prod(p.size()) for p in model_parameters])
# print(params)
