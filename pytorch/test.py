import torch

import model
import train


gen = model.Generator()
embed = model.Embedding()
disc = model.PiecewiseLinear(100, 100)
gen.init_weights()
embed.init_weights()
disc.init_weights()

loss = train.EnergyLossL2(128, torch.device("cpu"))
print(loss.ideal_value)

print(loss.loss(torch.normal(0, 1, (128, 100)),
                torch.normal(0, 1, (128, 100))))
train.train(gen, embed, disc, [[torch.ones((128, 3, 64, 64))]],
            128, 2, torch.device("cpu"))
