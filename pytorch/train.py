import torch
import torch.optim as opt
import torch.nn.functional as F
import torchvision.utils as vutils
import numpy as np
import matplotlib.pyplot as plt
import math


def train(G, E, D, data, batch_size, epochs, device, lr=0.0002, beta1=0.5, constraint_level=1, hinge_loss=0):
    assert(batch_size % 4 == 0)
    print("Beginning training!")
    fake_rng = torch.distributions.normal.Normal(
        0, 1).expand((batch_size, 100, 1, 1))

    optG = opt.Adam(G.parameters(), lr=lr, betas=(beta1, 0.999))
    optD = opt.Adam(list(E.parameters()) + list(D.parameters()),
                    lr=lr, betas=(beta1, 0.999))

    G.train()
    E.train()
    D.train()

    fixed_noise = (torch.normal(0, 1, (36, 100, 1, 1)).to(device))

    loss_fn = EnergyLossL2(batch_size, device)

    G_losses = []
    D_losses = []
    img_list = []

    for epoch in range(epochs):
        print("begin epoch ", epoch)
        for (i, data_batch) in enumerate(data):
            if data_batch[0].size()[0] < batch_size:
                break
            device_batch = data_batch[0].to(device)

            optD.zero_grad()

            real_embedding = E(device_batch)
            real_output = D(real_embedding)

            fake_batch = (fake_rng.sample().to(device))

            fakes = G(fake_batch)

            fakes_embedding = E(fakes)

            energy_dist = loss_fn.loss(
                real_embedding, fakes_embedding)

            fake_output = D(fakes_embedding)

            # D_loss = -torch.mean(torch.clamp(torch.log(1-(fake_output)), -100, 100)
            #                     ) - torch.mean(torch.clamp(torch.log(real_output), -100, 100))
            D_loss = loss_hinge_dis(fake_output, real_output)
            D_total_loss = D_loss + constraint_level*energy_dist
            D_total_loss.backward(retain_graph=True)
            optD.step()
            D_x = torch.mean(real_output).item()
            D_G_z1 = torch.mean(fake_output).item()

            G.zero_grad()
            # fakes = G(fake_batch)
            fakes_embedding = E(fakes)
            fake_output = D(fakes_embedding)
            G_loss = torch.mean(torch.norm(
                fakes_embedding-torch.reshape(fake_batch, (-1, 100)), dim=1)) + loss_hinge_gen(fake_output)
            G_loss.backward()
            optG.step()
            D_G_z2 = torch.mean(fake_output).item()

            # Output training stats
            if i % 50 == 0:
                print('[%d/%d][%d/%d]\tLoss_D: %.4f\tEnergy: %.4f\tLoss_G: %.4f\tD(x): %.4f\tD(G(z)): %.4f / %.4f'
                      % (epoch, epochs, i, len(data),
                         D_loss.item(), energy_dist.item(), G_loss.item(), D_x, D_G_z1, D_G_z2))

            # Save Losses for plotting later
            G_losses.append(G_loss.item())
            D_losses.append(D_loss.item())

        # Check how the generator is doing by saving G's output on fixed_noise
            if (i == 0) or ((epoch == epochs-1) and (i == len(data)-1)):
                G.eval()
                with torch.no_grad():
                    fake = G(fixed_noise).detach().cpu()
                new_img = vutils.make_grid(
                    fake, padding=2, normalize=False, nrow=6)
                img_list.append(new_img)
                plt.close()
                plt.figure(figsize=(16, 16))
                plt.imshow(np.transpose(new_img, (1, 2, 0)))
                plt.show()
                G.train()

    return (G_losses, D_losses, img_list)


def energy_loss(reals, fakes, batch_size):
    def compare_gaussian(x):
        return math.sqrt(2/math.pi)*torch.exp(-x**2/2) + \
            x*torch.erf(x/math.sqrt(2))

    # 2E|x-normal| = E|real-normal| + E|fake - normal|
    compare_with_gaussian = torch.mean(
        compare_gaussian(reals)+compare_gaussian(fakes), 0)

    # 2E|reals-fakes|
    compare_self_reals_fakes = 2*torch.mean(
        torch.abs(reals[0:batch_size//2]-fakes[0:batch_size//2]), 0)
    # E|reals-reals|
    compare_self_reals = torch.mean(torch.abs(
        reals[batch_size//2:3*batch_size//4] - reals[3*batch_size//4:batch_size]), 0)
    # E|fakes-fakes|
    compare_self_fakes = torch.mean(torch.abs(
        fakes[batch_size//2:3*batch_size//4] - fakes[3*batch_size//4:batch_size]), 0)

    compare_self = compare_self_reals_fakes + \
        compare_self_reals + compare_self_fakes

    # E|normal-normal| = 2/sqrt(pi)
    compare_self_gaussian = 2/math.sqrt(math.pi)

    return torch.sum(compare_with_gaussian - compare_self/4 - compare_self_gaussian)


class EnergyLossL2:
    def __init__(self, batch_size, device):
        self.batch_size = batch_size
        self.device = device

        normals1 = (torch.normal(0, 1, (10000, 100)).to(device))
        normals2 = (torch.normal(0, 1, (10000, 100)).to(device))
        self.ideal_value = torch.mean(
            torch.norm(normals1-normals2, dim=1)).item()

    def compare_gaussian(self, x):
        normals = (torch.normal(
            0, 1, (self.batch_size, 100)).to(self.device))
        return torch.norm(x-normals, dim=1)

    def loss(self, reals, fakes):

        # 2E|x-normal| = E|real-normal| + E|fake - normal|
        compare_with_gaussian = torch.mean(
            self.compare_gaussian(reals)+self.compare_gaussian(fakes), 0)

        # 2E|reals-fakes|
        compare_self_reals_fakes = 2*torch.mean(
            torch.norm(reals[0:self.batch_size//2]-fakes[0:self.batch_size//2], dim=1), 0)
        # E|reals-reals|
        compare_self_reals = torch.mean(torch.norm(
            reals[self.batch_size//2:3*self.batch_size//4] - reals[3*self.batch_size//4:self.batch_size], dim=1), 0)
        # E|fakes-fakes|
        compare_self_fakes = torch.mean(torch.norm(
            fakes[self.batch_size//2:3*self.batch_size//4] - fakes[3*self.batch_size//4:self.batch_size], dim=1), 0)

        compare_self = compare_self_reals_fakes + \
            compare_self_reals + compare_self_fakes

        # E|normal-normal| = 2/sqrt(pi)
        compare_self_gaussian = self.ideal_value

        return torch.sum(compare_with_gaussian - compare_self/4 - compare_self_gaussian)


def loss_hinge_dis(dis_fake, dis_real):
    loss = torch.mean(F.relu(1. - dis_real))
    loss += torch.mean(F.relu(1. + dis_fake))
    return loss


def loss_hinge_gen(dis_fake):
    loss = -torch.mean(dis_fake)
    return loss


def loss_dcgan_dis(dis_fake, dis_real):
    L1 = torch.mean(F.softplus(-dis_real))
    L2 = torch.mean(F.softplus(dis_fake))
    return L1 + L2


def loss_dcgan_gen(dis_fake):
    loss = torch.mean(F.softplus(-dis_fake))
    return loss
