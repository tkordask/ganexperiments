import tensorflow as tf
from tensorflow.keras import layers


def make_generator():
    model = tf.keras.Sequential()

    model.add(layers.Dense(4*4*8*64, use_bias=False, input_shape=(100,)))
    model.add(layers.BatchNormalization())
    model.add(layers.ReLU())
    model.add(layers.Reshape(target_shape=(4, 4, 8*64)))

    model.add(layers.Conv2DTranspose(
        64*4, 4, strides=2, padding='same', use_bias=False))
    model.add(layers.BatchNormalization())
    model.add(layers.ReLU())

    model.add(layers.Conv2DTranspose(
        64*2, 4, strides=2, padding='same', use_bias=False))
    model.add(layers.BatchNormalization())
    model.add(layers.ReLU())

    model.add(layers.Conv2DTranspose(
        64, 4, strides=2, padding='same', use_bias=False))
    model.add(layers.BatchNormalization())
    model.add(layers.ReLU())

    model.add(layers.Conv2DTranspose(3, 4, strides=2,
                                     padding='same', use_bias=False, activation='sigmoid'))

    return model


def make_discriminator():
    model = tf.keras.Sequential()
    model.add(layers.Conv2D(64, 4, strides=2,
                            input_shape=(64, 64, 3), padding='same'))
    model.add(layers.ReLU())

    model.add(layers.Conv2D(64*2, 4, strides=2, padding='same'))
    model.add(layers.ReLU())
    model.add(layers.BatchNormalization())

    model.add(layers.Conv2D(64*4, 4, strides=2, padding='same'))
    model.add(layers.ReLU())
    model.add(layers.BatchNormalization())

    model.add(layers.Conv2D(64*8, 4, strides=2, padding='same'))
    model.add(layers.ReLU())
    model.add(layers.BatchNormalization())

    model.add(layers.Conv2D(100, 4, strides=1, padding='valid'))

    model.add(layers.Flatten())
    model.add(layers.Dense(1))

    return model


cross_entropy = tf.keras.losses.BinaryCrossentropy(from_logits=True)


def discriminator_loss(real_output, fake_output):
    real_loss = cross_entropy(tf.ones_like(real_output), real_output)
    fake_loss = cross_entropy(tf.zeros_like(fake_output), fake_output)
    total_loss = real_loss + fake_loss
    return total_loss


def generator_loss(fake_output):
    return cross_entropy(tf.ones_like(fake_output), fake_output)
